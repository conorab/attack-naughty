# attack-naughty

An old script made back in 2016 which attempts determine which machine on your local flat network is causing your ping to go up by DOS'ing it and seeing if the ping has changed. This is an old and un-maintained script and so it kept largely for the memories. Note that using this on networks and computers you do not control may be illegal and so realistically has little-to-no utility outside an interesting proof-of-concept.
